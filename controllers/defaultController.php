<?php

namespace controllers;

use engine\controllers\Base;
use components\App;
use memento\User;

use components\Statistic;
use components\Randomizer;

class defaultController extends Base
{
    public function indexAction()
    {
        $app = App::getInstance();
        $user = User::findById($app->session->get('id'));
        $name = '';
        if(!empty($user))
            $name = $user['username'];

        $this->view->render('default/index', ['name' => $name]);
    }

    public function Homework2Action()
{
    echo Statistic::GetTime().' time micro secund <br>';// если статическая функция то :: обращение к функции без создания класса!! если она не static то надо создавать объект для ее передачи!!
    echo Statistic::GetMemory().' memory in byte <br>';//Statistic::GetMemory() и memory_get_usage(true) одинаково
    var_dump(Randomizer::arrDir()); //$root = (arrDir()); полный вывод информации
    echo memory_get_usage(true).' memory in byte <br>';
    echo microtime().' time micro secund ';
}

    public function viewAction()
    {
        echo '<h1>views</h1>';
    }

    public function errorAction()
    {
        echo 'Error';
    }
}