<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 10.09.16
 * Time: 14:14
 */

namespace controllers;

use engine\controllers\Base;
use components\App;
use memento\User;

class userController extends Base
{
    public function loginAction()
    {
        $app = App::getInstance();
        $app->session->set('id', null);
        if($app->request->isPost){
            if(isset($_POST['email'])&&isset($_POST['password'])){
                $result = User::findByEmail($_POST['email']);
                $result_two = User::findByPassword($_POST['password']);
                if(!empty($result)&&!empty($result_two)){
                    $app->session->set('id', $result['id']);
                    $app->request->redirectTo('/');
                }
            }
        }

        $this->view->render('user/login');
    }
}