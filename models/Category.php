<?php
namespace models;


class Category
{
    public $id;
    
    public $name;
    
    public $code;

    public function __construct($id, $code, $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->code = $code;
    }

}