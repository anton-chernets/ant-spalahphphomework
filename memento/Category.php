<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 20.08.16
 * Time: 15:57
 */

namespace memento;


class Category
{
    public static function get()
    {
        return [
            ['id' => 1, 'code' => 'ANM', 'name' => 'Animals'],
            ['id' => 2, 'code' => 'HOME', 'name' => 'Home'],
            ['id' => 3, 'code' => 'PEOPLE', 'name' => 'Peoples']
        ];
    }
}