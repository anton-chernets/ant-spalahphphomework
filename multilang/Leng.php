<?php

namespace multilang;

class Leng //так делается логирование и мультиязычность
{
//выбираем здесь уже из транслейта выход от сессии что денис даст
public static $lang = 'ru';

public static $memento = array(
			"en" => array(
				"hello" => "Hello",
				"my_name" => "My name is",
				"homework_finish" => "Completed homework",
				"homework2" => "My home work second",
				"select_lang" => "language selection",
				"select_lang_value" => "language selection value",
				"send" => "send",
				"b_p" => "Base PAGE"
				),
			"ru"=> array(
				"hello" => "Привет", 
				"my_name" => "Меня зовут",
				"homework_finish" => "Выполненные домашние работы",
				"homework2" => "Второе домашнее задание",
				"select_lang" => "Выбор языка",
				"select_lang_value" => "Выбор языка значение",
				"send" => "отправить",
				"b_p" => "Базовая СТРАНИЦА"
				)
		);

	public static function translate($word)
	{
	//здесь примим переменную из сессии
		if (!empty(self::$memento[self::$lang])){
			if(!empty(self::$memento[self::$lang][$word]))
				return self::$memento[self::$lang][$word];
		}
		return $word;
	}
}