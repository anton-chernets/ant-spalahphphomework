<?php
namespace components; // namespase соединяет имя папки и файла(класса). Название файла и класса должно быть одинаковыми.

class Statistic{

	static function  GetMemory(){
		return memory_get_usage(true);//замер памяти
	}

	static function  GetTime(){
		return microtime();//замер времени до микро
	}

}