<?php
namespace components;

class App
{
    protected static $instance = null;

    protected $isDebug = false;

    public $reference;

    public $request;

    public $session;

    public $basePath;

    public $db;//для БД

    public function __construct($basePath)
    {
        //echo 'begin<br>';
        $this->basePath = $basePath;
        $this->reference = new Reference();
        $this->request  = new Request();
        $this->session = new Session();
        $this->db = new Connector('127.0.0.1', 'root', '', 'sb.blog');//для БД
        $result = $this->db->setSql('SELECT * FROM users WHERE id = 2')->one();//для БД запуск строк из таблицы - один элемент ('SELECT*FROM users /*WHERE id=1*/')
        $result2 = $this->db->setSql('SELECT * FROM users')->all();//для БД запуск строк из таблицы все элементы мы получим на выходе два массива - в дебагере все и один в двух разных массивах


        self::$instance = $this;
    }

    /**
     * @return App
    */
    public static function getInstance()
    {
        return self::$instance;
    }

    /**
     * @return int
     */
    public function run()
    {
       // echo 'run<br>';

        $namespace = $this->request->getControllerNamespace();
        if(empty($namespace)){
            $controller = $this->getController('\controllers\defaultController');
            $this->runAction($controller, $controller->defaultAction);
        }elseif(class_exists($namespace)){
            $controller = $this->getController($namespace);
            $this->runAction($controller, $this->request->getAction());
        }else{
            $controller = $this->getController('\controllers\defaultController');
            $this->runAction($controller, 'error');
        }

        return $this->end(0);
    }
    
    /**
     * @param string $name
     * @return \controllers\DefaultController
    */
    protected function getController($name)
    {
        if(empty($name))
            return null;
        $view = new View($this->basePath);
        return new $name($view);
    }

    /**
     * @param \controllers\DefaultController $controller
     * @param string $action
     * @return mixed
     */
    protected function runAction($controller, $action)
    {
        if(empty($action)){
            $controller->{$controller->defaultAction.'Action'}();
        }elseif(method_exists($controller, $action.'Action')){
            $controller->{$action.'Action'}();
        }else{
            $controller = $this->getController('\controllers\defaultController');
            $this->runAction($controller, 'error');
        }

    }

    /**
     * @param int $code
     * @return int
     */
    protected function end($code)
    {
       // echo 'end<br>';
        return $code;
    }
}
