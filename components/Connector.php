<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 24.09.16
 * Time: 16:01
 */

namespace components;


class Connector
{
    public $host;

    public $user;

    public $password;

    public $db_name;

    public $port;

    public $socked;//�� ������������

    protected $db;
    
    private $_sql;

    public function __construct($host, $user, $password, $db_name, $port = 3306)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->db_name = $db_name;
        $this->port = $port;
        $this->db = new \mysqli($this->host, $this->user, $this->password, $this->db_name, $this->port);
    }

    public function setSql($str)
    {
        $this->_sql = $str;
        return $this;
    }
    
    public function one()
    {
        $result = $this->db->query($this->_sql);
        return $result->fetch_assoc();
    }
    
    public function all()
    {
        $result = $this->db->query($this->_sql);
        return $result->fetch_all();
    }

}