<?php
namespace components;


class Request
{
    protected $_userAgent;

    protected $_lang;

    protected $_remoteIP;

    protected $_scheme;

    protected $_method;

    protected $_queryString;

    protected $_queryURI;

    protected $_time;

    protected $_class = [];

    /**
     * 
    */
    public $isGet = false;
    
    public $isPost = false;
    
    public $isDelete = false;
    
    public $isPut = false;

    public function __construct()
    {
        $this->_userAgent = $_SERVER['HTTP_USER_AGENT'];
        $this->_lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        $this->_remoteIP = $_SERVER['REMOTE_ADDR'];
        $this->_scheme = $_SERVER['REQUEST_SCHEME'];
        $this->_method = $_SERVER['REQUEST_METHOD'];
        $this->_queryString = $_SERVER['QUERY_STRING'];
        $this->_queryURI = $_SERVER['REQUEST_URI'];
        $this->_time = $_SERVER['REQUEST_TIME'];
        
        $this->parseMethod();
    }
    
    protected function parseMethod()
    {
        switch ($this->_method){
            case "GET" : $this->isGet = true; 
                break;
            case "POST" : $this->isPost = true; 
                break;
            case "PUT" : $this->isPut = true;
                break;
            case "DELETE" : $this->isDelete = true; 
                break;
            default: break;
        }
    }

    /**
     * @return string
    */
    public function getUserAgent()
    {
        return $this->_userAgent;
    }

    /**
     * @return string
    */
    public function getControllerNamespace()
    {
        $url = substr($this->_queryURI, 1);
        if(!empty($url)){
            $arr = explode('/', $url);
            $controllerName = strtolower($arr[0]);            
            return '\\controllers\\'.$controllerName.'Controller';
        }
        return null;
    }

    /**
     * @return string
    */
    public function getAction()
    {
        $url = substr($this->_queryURI, 1);
        if(!empty($url)){
            $arr = explode('/', $url);
            if(isset($arr[1])){
                $action = strtolower($arr[1]);
                return $action;
            }
        }
        return null;
    }

    /**
     * @param string $url
    */
    public function redirectTo($url)
    {
        header("Location: ".$url);
    }
}
