<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 10.09.16
 * Time: 14:32
 */
?>

<h1> Base PAGE </h1>
<?php

use multilang\Leng;

$title = 'My first php projekt';
$now = date('d.m.Y H:i');

$HTML = '
<html>
<meta charset="utf-8">
<title>'.$title.'</title>
<body>
	<br><br>
	<b>'.Leng::translate('select_lang').':</b>
	<br><br>
	<form method="POST" action="">
		<select name="languageSelection">
			<option value="en" selected>English</option>
			<option value="ru">Russian</option>
		</select>
		<br><br>
		<input type="submit" value="'.Leng::translate('send').'">
	</form>

	<table width="100%" height="15%" border=0>
		<tr valign="middle">
			<td align="center">Today: '.$now.'
			</td>
		</tr>
	</table>
</body>
</html>';
print $HTML;

echo "<p>".Leng::translate('select_lang_value').": <b>".$_POST['languageSelection']."</b></p>";
echo '<b><br>'.Leng::translate('homework_finish').':</b><br><a href="http://mysite.ru/default/Homework2">'.Leng::translate('homework2').'</a><br>';
?>


<?php if(!empty($name)) : ?>
	<h2>Hello <?= $name?></h2>

<?php endif; ?>
